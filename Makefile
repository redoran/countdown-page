REG=reg.terada.pl:5000

build:
	docker build -t redoran-countdown .
	docker tag redoran-countdown ${REG}/redoran-countdown:latest
	docker push ${REG}/redoran-countdown:latest
