var app = new Vue({
  data: {
    countdown: 0,
    reducer: 0,
    interval: undefined,
    partners: {
      "mmorank": "http://mmorank.pl",
      "portalmmo": "http://www.portalmmo.pl",
      "viaww": "http://viawwwgamers.pl",
      "yggdrasil": "http://yggdrasil.pl",
    }
  },
  el: '#app',
  methods: {
    leading0: function(str) {
      if (("" + str).length == 1) {
        return '0' + str;
      } else {
        return str;
      }
    }
  },
  computed: {
    'seconds': function() {
      return this.leading0(parseInt(this.countdown % 60));

    },
    'minutes': function() {
      return this.leading0(parseInt(this.countdown / 60) % 60);

    },
    'hours': function() {
      return this.leading0(parseInt(this.countdown / 3600) % 24);

    },
    'days': function() {
      return this.leading0(parseInt(this.countdown / 86400));
    }
  },
  created: function() {
    var $vm = this;
    $vm.countdown = (new Date($("#launch-date").data('when')).getTime() - new Date().getTime()) / 1000;
    $vm.interval = setInterval(function() {
      $vm.countdown--;
      if ($vm.countdown === 0) {
        clearInterval($vm.interval);
      }
    }, 1000);

  }
});
